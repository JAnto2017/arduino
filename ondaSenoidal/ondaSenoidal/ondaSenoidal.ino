#define MIN_T 1000  //min duración periodo
#define MAX_T 10000 //max duración periodo
#define POT A0
//---------------------------------------------------------
unsigned long T;
unsigned long_delay;
double angulo;      //Ángulo en radianes de 0 a 2 pi
double deltaAngulo; 
double pasos;
double _delay;
//---------------------------------------------------------
void setup() {
  Serial.begin(115200);
  while(!Serial){
    
  }
}
//---------------------------------------------------------
void loop() {
  angulo = 0;
  T = map(analogRead(POT),0,1023,MIN_T,MAX_T);
  pasos = T/80.0;
  _delay = T / pasos;
  deltaAngulo = 2.0 * PI / pasos;
  while(angulo < 2 * PI){
    Serial.print(int(sin(angulo)*128+127)); //calculomos el seno
    Serial.print(" 128\n");
    angulo += deltaAngulo;
    delay(_delay);
  }
}
