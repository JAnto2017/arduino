/*
 * Conversión onda senoidal de amplitud unitaria y periodo de 6 segundos 
 * en onda cuadrada tipo PWM.
 * Se puede modificar la frecuencia de la onda senoidal y medir con el osciloscopio
 * la forma de la onda PWM.
 */

//-------------------------------------------------------
int pin3 = 3; //salida PWM
float dato;   //registra información función senoidal
float t=0;    //contabiliza el tiempo
//-------------------------------------------------------
void setup() {
  Serial.begin(9600);
  pinMode(pin3,OUTPUT);
}
//-------------------------------------------------------
void loop() {
  dato=sin(2*M_PI*199*t/2); //función senoidal sen(2·pi3t/6)
  if(dato >=0)
    analogWrite(pin3,255);
  else
    analogWrite(pin3,0);
  delay(2);

  t=t+0.002;  //incremento de tiempo 2ms
}
