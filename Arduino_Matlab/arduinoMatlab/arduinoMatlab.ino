/*
 * Comunicación con MATLAB - Representación gráfica
 */
//----------------------------------------------------------
#include<math.h>
float y;
float t=0;
//----------------------------------------------------------
void envia_datos(float x, float y){
  Serial.print(y);
  Serial.print(" ");
  Serial.println(x);
}
//----------------------------------------------------------
void setup() {
  Serial.begin(9600);
}
//----------------------------------------------------------
void loop() {
  //varios funciones
  /*
   * y=1-pow(2.37,-t);
   * y=t/sqrt(1+t*t);
   * y=tanh(t);
   * y=t/(1+abs(t));
   * y=2*t/(1+t*t);
   * y=sinh(t);
   * y=(1-pow(2.27,-t*t))*t;
   * y=t*t*t*t*sin(t);
   */
  y = sin(t);
  envia_datos(t,y); //tx a MATLAB
  t = t+0.01;
  delay(10);
}
