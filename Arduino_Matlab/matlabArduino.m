%Comunicaci�n Arduino - MATLAB
%---------------------------------------
%necesita de >> pkg load instrument-control Octave
clear all
close all
clc
format short
%crea el objeto serie - serial(�\\.\COM15�)
%canal_serie = serial('COM4','BaudRate',9600,'Terminator','CR/LF');
canal_serie = serial('COM4');
%set(canal_serie, 'baudrate', 9600);     % See List Below
%set(canal_serie, 'bytesize', 8);        % 5, 6, 7 or 8
%set(canal_serie, 'parity', 'n');        % 'n' or 'y'
%set(canal_serie, 'stopbits', 1);        % 1 or 2
%set(canal_serie, 'timeout', 123);     % 12.3 Seconds as an example here
fopen(canal_serie);
xlabel('Segundos'); ylabel('Datos'); title('Adquisici�n de datos Arduino');
grid on; hold on;

prop = line(nan,nan,'Color','b','LineWidth',1);
datos = fscanf(canal_serie,'%f %f', [2,1]); %sincron�a datos puerto serie
clc;

disp('Adquisici�n datos Arduino');
i=1;

while i<1000
  datos =  fscanf(canal_serie,'%f %f',[2,1]);
  y(i) = datos(1,1);
  x(i) = datos(2,1);
  set(prop,'YData',y(1:i),'XData',x(1:i));
  drawnow;
  i=i+1;
endwhile

fclose(canal_serie);
delete(canal_serie);
clear canal_serie;
