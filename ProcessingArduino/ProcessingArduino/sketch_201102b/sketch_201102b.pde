import processing.serial.*; // invocar la librería Serial
 
Serial puerto; // se declara una variable para la com. serial
 
void setup()
{
 puerto = new Serial(this,"COM4", 9600); 
 // se le asigna el puerto COM6. En este caso, lo vi 
 // en el administrador de dispositivos (windows)
 size(300,150); // se crea una ventana de 300 píxeles de ancho
                // por 150 píxeles de alto
}
 
void draw()
{
 background(0,0,0); // fondo en formato RGB = blanco
 // ------------------------------------------------- //
 fill(0,204,51);         // color del texto
 textSize(16);           // tamaño del texto
 textAlign(CENTER, TOP); // alineación del texto
 text("Led interno del Arduino",150,25); // texto, posición X,Y
 stroke(90);        // línea bordeada
 strokeWeight(2);   // grosor de la línea bordeada
 // ---------- CUADRO 1 ---------- //
 fill(255,255,255);      // relleno del cuadro 1
 rect(50,60,80,40,10);   // (A,B,C,D,E) 
 // A = Posición en X ~~ B = Posición en Y 
 // C = Ancho del rectángulo ~~ D = Alto del rectángulo 
 // E = Borde del rectángulo
 fill(0,0,0);            // color del texto = blanco
 text("ON", 90, 70);     // texto, posición X,Y
 // ---------- CUADRO 2 ---------- //
 fill(255,255,255);      // relleno del cuadro 2
 rect(175,60,80,40,10);  // texto, posición X,Y
 fill(0,0,0);            // color del texto = blanco
 text("OFF", 215, 70);   // texto, posición X,Y
}
 
void mousePressed()
{
 if((mouseX>50 & mouseX<130) & (mouseY>60 & mouseY<100))
 // evaluar si el botón de "ON" es pulsado
 // EjeX"ON"= 50(inicio del cuadro) + 80(largo del cuadro)= 130
 // EjeY"ON"= 60(inicio del cuadro) + 40(alto del cuadro)= 100
 // mouseX= coordinada en X del cursor
 // mouseY= coordinada en Y del cursor
 {
 fill(60,60,60);           // color del botón al ser presionado
 rect(50,60,80,40,10);     // crear rectángulo 1
 puerto.write('E');
 // enviar la letra "E" por el puerto creado para que el
 // carácter sea recibido por el puerto serial del Arduino
 // y de esta forma encender el led.
 println("Led ENCENDIDO"); // imprimir en pantalla estado del
                           // led para verificar que está
                           // ejecutando la función
 }
 
  if((mouseX>175 & mouseX<255) & (mouseY>60 & mouseY<100))
 // evaluar si el botón de "OFF" es pulsado
 // EjeX"OFF"= 175(inicio del cuadro) + 80(largo del cuadro)= 255
 // EjeY"OFF"= 60(inicio del cuadro) + 40(alto del cuadro)= 100
 // mouseX= coordinada en X del cursor
 // mouseY= coordinada en Y del cursor
 {
  fill(60,60,60);         // color del botón al ser presionado
  rect(175,60,80,40,10);  // crear rectángulo 2
  puerto.write('A');
  // enviar la letra "A" por el puerto creado para que el
  // carácter sea recibido por el puerto serial del Arduino
  // y de esta forma apagar el led.
  println("Led APAGADO"); // imprimir en pantalla estado del
                          // led para verificar que está
                          // ejecutando la función
 }
}
