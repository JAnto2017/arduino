const int led = 13; // declaramos el pin 13 como led
char valor = 0;     // usamos una constante tipo char, ya
                    // que van a ser letras los caracteres
                    // que se van a enviar
 
void setup() 
{
 Serial.begin(9600);    // configura el puerto serial con una
                        // velocidad de comunicación de 9600bps
 pinMode(led,OUTPUT);   // configura led (pin13) como salida
 digitalWrite(led,LOW); // se coloca led con el estado en 
                        // bajo para iniciar
}
 
void loop() 
{
 if (Serial.available() > 0) // se abre el puerto serial 
                             // leyendo los datos de entrada
 {
  valor = Serial.read();   // el valor recibido por el puerto
                           // serial se almacena en la
                           // variable "valor"
  if (valor == 'E')        // se compara el valor recibido 
                           // con la letra "E"
  {                        // si el valor es igual a "E"
   digitalWrite(led,HIGH); // enciende el led (pin13)
  }
  else if (valor == 'A') // sino se compara el valor recibido
                         // con la letra "A"
  {                      // si el valor es igual a "A"
   digitalWrite(led,LOW);// apaga el led (pin13)
  }
   else                  // de lo contrario si se introduce
  {                      // un caracter que no sea "E" ni "A"
   Serial.println("Caracter NO PERMITIDO");  // imprime
                                             // vía serial
  }
 }
}
