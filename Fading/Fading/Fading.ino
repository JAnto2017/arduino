/*
 * This example shows how to fade and LED 
 * using the analogWrite() function.
 * NANO : no funciona.
 * MEGA 2560 : si funciona.
 * LED en pin 9 con resistencia de 220 Ohm.
 */
int ledPin = 17;
void setup() {}
//-------------------------------------------------
void loop() {
  // fade in from min to max in increments of 5 points:
  for(int fadeValue=0;fadeValue<=255;fadeValue+=5){
    // sets the value (range from 0 to 255)
    analogWrite(ledPin,fadeValue);
    // wait for 30 ms to see the dimming effect
    delay(30);
  }
  // fade out from max to min in increments of 5 points:
  for(int fadeValue=255;fadeValue>=0;fadeValue-=5){
    // sets the value (range from 0 to 255)
    analogWrite(ledPin,fadeValue);
    // wait for 30 ms to see the dimming effect
    delay(30);
  }
}
