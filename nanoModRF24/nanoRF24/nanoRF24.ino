#include <RF24.h>
#include <nRF24L01.h>
#include <printf.h>
#include <RF24_config.h>

#include <SPI.h>


//-------------------------------------------------------
RF24 radio(7,8);
const byte identificacion[6]="00001";
//-------------------------------------------------------
void setup() {
  radio.begin();
  radio.openWritingPipe(identificacion);
  radio.setPALevel(RF24_PA_MIN);
  radio.stopListening();          //se configura como Tx
}
//------------------------------------------------------
void loop() {
  const char texto[] = "hola mundo";
  radio.write(&texto,sizeof(texto));
  delay(1000);
}
